import test
from PIL import Image





def CombineCanvas(outName):
    print("> Combining downloaded images...")

    img1 = Image.open('H47V13.png')
    img2 = Image.open('H47V14.png')
    img3 = Image.open('H48V13.png')
    img4 = Image.open('H48V14.png')

    width, height = max(img1.width, img2.width, img3.width, img4.width), max(img1.height, img2.height, img3.height, img4.height)

    width *= 2
    height *= 2

    final_img = Image.new('RGB', (width, height))
    final_img.paste(img1, (0, 0))
    final_img.paste(img2, (0, height//2))
    final_img.paste(img3, (width//2, 0))
    final_img.paste(img4, (width//2, height//2))
    final_img.save(outName)
    print("> Combined sucessfull!")


def Crop(whatName, resultName, left, upper, right, lower):
    print("> Croping the image...")

    tocrop = Image.open(whatName)

    crop_box = (left, upper, right, lower)

    cropped_img = tocrop.crop(crop_box)

    cropped_img.save(resultName)
    print("> Crop sucessfull!")

def FillW(fileName, result, left, upper, right, lower, fill_color):
    print("> Filling the space...")
    img = Image.open(fileName)
    fill_img = Image.new('RGB', (right-left, lower-upper), fill_color)
    img.paste(fill_img, (left, upper))
    img.save(result)
    print("> Filling sucessfull!")

def GetNewCanvas(outputName):
    print(f"! Creating {outputName} canvas...")
    test.download()
    CombineCanvas(outputName)
    Crop(outputName, outputName, 91, 194, 318, 331)
    FillW(outputName, outputName, 132, 105, 227, 137, (255, 255, 255))
    FillW(outputName, outputName, 0, 113, 20, 227, (255, 255, 255))
    FillW(outputName, outputName, 20, 122, 30, 227, (255, 255, 255))
    print(f"! {outputName} canvas created!\n")

def NewSample(sampleName):
    GetNewCanvas(sampleName)



def FindAttack(sample, canvas):
    print("> Checking for attacks...")
    attacked = False
    img1 = Image.open(sample)
    img2 = Image.open(canvas)

    for x in range(img1.width):
        if (attacked): break
        for y in range(img1.height):
            if (attacked): break
            pixel1 = img1.getpixel((x, y))
            pixel2 = img2.getpixel((x, y))
            if (pixel1 != pixel2):
                attacked = True
                print(f"We've got attacked at {x}:{y}!!!")

    if(attacked): print("ALARM!!!")
    else: print("> Everything is fine yet.")
    return attacked


def FalseAttack(target, x,y):
    print("> Making false attack...")
    img = Image.open(target)
    color = (255, 0, 0)
    img.putpixel((x,y), color)
    img.save(target)
    print(f"> False attack at {x}:{y} is successful!")




#NewSample("sample.jpg")

def RegularCheck():
    GetNewCanvas("canv.jpg")


    if(FindAttack("sample.jpg", "canv.jpg")):
        print("We are attacked!")
        return True
    else:
        print("We are not attacked!")
        return False
    


