import requests
from bs4 import BeautifulSoup


def download():
    print("* Starting downloading of images...")
    for a in range(47, 49):
        for b in range(13,15):
            link2 = f"https://pixelplanet.fun/tiles/0/6/{a}/{b}.webp"
            response = requests.get(link2)
            if(response.ok):
                print(f"H{a}V{b}.webpp success")
                image = response.content
                with open(f"H{a}V{b}.png", 'wb') as file:
                        file.write(image)
            else:
                print(f"H{a}V{b} Not found!")
                continue


